import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Image,
  TouchableOpacity,
  Animated,
  PanResponder,
  Modal,
  Alert
} from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import ImageResizer from 'react-native-image-resizer';
import EditButtons from './../cardItem/EditButtons';


export default class EditPhotoPage extends Component {

  constructor() {
    super();
    this.interval;
    this.is_finished = false;
    this.state = {
      movableImage: 'http://pngimg.com/uploads/chair/chair_PNG6908.png',
      height: 30,
      width: 30
  }
}

  componentWillMount() {
    console.log('kanchvec componentWillMount');
    this.animatedValue = new Animated.ValueXY();
    this._value = {x: 0, y: 0};

    this.animatedValue.addListener((value) => this._value = value);
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onPanResponderGrant: (e, gestureState) => {
        this.animatedValue.setOffset({
          x: this._value.x,
          y: this._value.y,
        })
        this.animatedValue.setValue({x: 0, y: 0})
      },
      onPanResponderMove: (e, gestureState) => {
        console.log('MOVE IT MOVE IT_______>', gestureState.numberActiveTouches , gestureState.moveX, gestureState.moveY);
         if(gestureState.moveX < 50 || gestureState.moveX > 300 || gestureState.moveY < 70 || gestureState.moveY > 500) {
           console.log('GESTURESTATE');
           return false;
         }
        Animated.event([
          null, { dx: this.animatedValue.x, dy: this.animatedValue.y},
        ])(e, gestureState);
      },
      onPanResponderRelease: () => {
            this.animatedValue.flattenOffset(); // Flatten the offset so it resets the default positioning
          }
    })
    //let temp = this.getParameters();
    //console.log('componentWillMount-um --->', temp.movableImage);
    //this.setState({movableImage: temp.movableImage})
  }

  getParameters() {
    let result = null;
    if(this.props.navigation.state.params.parameters) {
      result = {  width: this.props.navigation.state.params.parameters.width,
                  height: this.props.navigation.state.params.parameters.height,
                  movableImage: this.props.navigation.state.params.parameters.uri }
      return result;
  } else {
    result = {  width: 0,
                height: 0,
                movableImage: null}
      return result;
    }
  }

  zoomIn() {
    let newHeight = this.state.height + 1;
    let newWidth = this.state.width + 1;
    this.setState({height: newHeight, width: newWidth});
  }

  zoomInLong() {
    this.is_finished = true;
    this.interval = setInterval(() => {
    if (this.is_finished == true) {
      let newHeight = this.state.height + 1;
      let newWidth = this.state.width + 1;
      this.setState({height: newHeight, width: newWidth});
      }
    }, 200);
  }
  zoomInEnd() {
    this.is_finished = false;
    clearInterval(this.interval);
  }

  zoomOut() {
      let newHeight = this.state.height - 1;
      let newWidth = this.state.width - 1;
      this.setState({height: newHeight, width: newWidth});
  }

  zoomOutLong() {
    this.is_finished = true;
    this.interval = setInterval(() => {
    if (this.is_finished == true) {
      let newHeight = this.state.height - 1;
      let newWidth = this.state.width - 1;
      this.setState({height: newHeight, width: newWidth});
      }
    }, 200);
  }

  zoomOutEnd() {
    this.is_finished = false;
    clearInterval(this.interval);
  }

  render() {

    let temp = this.getParameters();

    let width = temp.width;
    let height = temp.height;
    console.log('renderum movableIMG------>', this.state.movableImage);
    const animatedStyle = {
      transform: this.animatedValue.getTranslateTransform()
    }
    const { params } = this.props.navigation.state;
    console.log('renderum params.source', params.source);

    return (
      <View style={{flex: 1}}>
          <Image style={styles.container} source={{uri: 'https://cdn.cliqueinc.com/posts/212361/-2030969-1483470364.640x0c.jpg'}}>

          <Animated.View style={animatedStyle} {... this.panResponder.panHandlers}>
              <Image style={{resizeMode: 'contain', height: responsiveHeight(this.state.height), width: responsiveWidth(this.state.width)}} source={{uri: this.state.movableImage}}/>
          </Animated.View>

          </Image>

          <View style={styles.buttonContainer}>
                <EditButtons logo={require('./../images/ImageShopButton.png')} onPress={() => this.props.navigation.navigate('Tab2')}/>
                <EditButtons logo={require('./../images/ImageSaveButton.png')}/>
                <EditButtons logo={require('./../images/ImageZoomInButton.png')} onPress={() => {this.zoomIn()}} onPressIn={() => {this.zoomInLong()}} onPressOut={() => {this.zoomInEnd()}}/>
                <EditButtons logo={require('./../images/ImageZoomOutButton.png')} onPress={() => {this.zoomOut()}} onPressIn={() => {this.zoomOutLong()}} onPressOut={() => {this.zoomOutEnd()}}/>
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: responsiveWidth(100),
    height: responsiveHeight(95),
    resizeMode: 'contain',
  },
  buttonContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: -5,
    height: responsiveHeight(10),
    width: responsiveWidth(100),
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'rgba(225,225,225,0.5)'
  },
  modal: {
    flex: 1,
    backgroundColor: 'rgba(225,225,225,0.5)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalContent: {
    width: responsiveWidth(80),
    height: responsiveHeight(30),
    backgroundColor: '#F4F6F7',
    justifyContent: 'space-between'
  },
  textInput: {
    height: responsiveHeight(6),
    width: responsiveWidth(30),
  },
  textHeader: {
    fontSize: responsiveFontSize(3),
    color: 'black',
    textAlign: 'center'
  },
  button: {
   width: responsiveWidth(20),
   height: responsiveHeight(5),
   justifyContent: 'center',
 },
})
